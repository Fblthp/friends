import httplib
import sys
import webbrowser
import os
import random
import urllib
from Crypto.Cipher import ARC4
import argparse

p = 1000000007
g = 17

conn = httplib.HTTPConnection('127.0.0.1', 89)
conn.connect()

parser = argparse.ArgumentParser()
parser.add_argument("--id", type=int, default=17)
parser.add_argument("--url", default="index.html")
parser.add_argument("--host", default="127.0.0.1")
parser.add_argument("--port", type=int, default=89)
args = parser.parse_args()

headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
id, key = args.id, ''
try:
	global key, id
	id, key = open('key', 'r').read().split()
	if (id == '' or id == -1 or key == ''):
		raise
except:
	# generate key, used for debuging
	global key, id
	a = random.randint(10, 1000)
	num = g ** a % p
	id = args.id

	params = urllib.urlencode({'id' : id, 'key_part' : num})

	conn.request("POST", "/", params, headers)
	response = conn.getresponse()
	data_received = response.read()
	key = int(data_received) ** a % p

	open('key', 'w').write(str(id) + ' ' + str(key))


# send request
params = urllib.urlencode({'id': id})

#encrypt
encrypter = ARC4.new(str(key))

encrypted_url = encrypter.encrypt(args.url).encode('hex')
conn.request("GET", encrypted_url, params, headers)


#response
rsp = conn.getresponse()
data_received = rsp.read()

#decrypt
decrypter = ARC4.new(str(key))
data_decrypted = decrypter.decrypt(data_received.decode("hex"))


#open
page = open('result.html', 'w')
page.write(data_decrypted)
page.close()

webbrowser.open_new('file://' + os.getcwd() + '/result.html')
conn.close()
