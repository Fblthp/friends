from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import os
import random
from cgi import parse_header, parse_multipart
from urlparse import parse_qs
from Crypto.Cipher import ARC4
import sqlite3
import argparse

p = 1000000007
g = 17


class FriendsHTTPRequestHandler(BaseHTTPRequestHandler):


    def get_vars(self):
        ctype, pdict = parse_header(self.headers['content-type'])
        length = int(self.headers['content-length'])
        postvars = parse_qs(self.rfile.read(length), keep_blank_values=1)
        return postvars


    def do_POST(self):
        vars = self.get_vars()
        id = int(vars['id'][0])
        required_id = (id, )
        db_id, db_key = "", ""
        for row in c.execute('SELECT * FROM users WHERE id = ?', required_id):
            db_id, db_key = row


        if (db_id == ""):
            key_part = int(vars['key_part'][0])
            self.gen_key(key_part, id)
            return

        self.send("-1")


    def gen_key(self, key_part, id):
        b = random.randint(10, 1000)
        res = g ** b % p
        key = key_part ** b % p

        vals = (id, str(key))
        c.execute('INSERT INTO users VALUES (?,?)', vals)
        conn.commit()

        self.send(res)


    def send(self, info):
        self.send_response(200)

        self.send_header('Content-type','text-html')
        self.end_headers()

        self.wfile.write(info)
        return


    def do_GET(self):
        vars = self.get_vars()
        id = int(vars['id'][0])
        required_id = (id, )
        db_id, key = "", ""
        for row in c.execute('SELECT * FROM users WHERE id = ?', required_id):
            db_id, key = row


        if (db_id == ""):
            print "no user with such id"
            return

        try:
            url = self.path
            decrypter = ARC4.new(str(key))
            url_decrypted = decrypter.decrypt(url.decode("hex"))
            f = open("html/" + url_decrypted)
            self.send_response(200)

            self.send_header('Content-type','text-html')
            self.end_headers()

            data = f.read()
            encrypter = ARC4.new(str(key))
            encrypted_data = encrypter.encrypt(data).encode('hex')
            self.wfile.write(encrypted_data)
            f.close()
            return
            
        except IOError:
            self.send_error(404, 'file not found')


parser = argparse.ArgumentParser()
parser.add_argument("--db", default="users.db")
parser.add_argument("--host", default="127.0.0.1")
parser.add_argument("--port", type=int, default=89)
args = parser.parse_args()

conn = sqlite3.connect(args.db)
c = conn.cursor()
try:
    c.execute("CREATE TABLE users (id int, key text)")
except:
    pass
conn.commit()

server_address = (args.host, args.port)
httpd = HTTPServer(server_address, FriendsHTTPRequestHandler)
httpd.serve_forever()
conn.close()

